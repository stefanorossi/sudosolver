private class Cella{
  private int R; //Riga della cella
  private int C; //Colonna della cella
  private int L; //lato
  private int N; //numero contenuto nella cella
  private color ColCella; // colore bg della cella
  private color ColN; //colore del numero
  private int pntFontN; //grnadezza carattere
  private color warningColor; //colore sfondo per evidenziare celle in conflitto col nuovo num da inserire
  private boolean fissa;
  private boolean selezionata; //true se la cella è stata cliccata
  
  //COSTRUTTORE
  public Cella(int R, int C, int L){
    this.R = R;
    this.C = C;
    this.L = L;
    
    this.N = 0;
    this.ColCella = color(0, 200, 200, 80); //Azzurro trasp
    this.warningColor = color(255, 0, 0, 0); //rosso completamente trasparente
    this.pntFontN = 20;
    this.selezionata = false;
    this.fissa = false;
    this.ColN = color(0, 102, 153); //blu chiaro
  }
  
  //METODI
  public void toggleFissa(){
    this.fissa = !this.fissa;
    //println(fissa);
    if(this.fissa){
      this.ColN = color(0, 0, 0); //nero
      this.ColCella = color(255, 128, 0, 200); //arancione
    }else{
      this.ColN = color(0, 102, 153); //blu chiaro
      this.ColCella = color(0, 200, 200, 80); //Azzurro trasp
    }
  }
  
  //GETTER E SETTER  
  public boolean isSelezionata() {
    return this.selezionata;
  }
  
  public void setSelezionata(boolean selezionata) {
    this.selezionata = selezionata;
  }
  
  public int getX() {
        return this.R * this.L;
  }
  public int getY() {
        return this.C * this.L;
  }
  
  public int getL() {
        return this.L;
  }
  
  public void setR(int R) {
        this.R = R;
  }
  
  public int getR() {
        return this.R;
  }
  
  public void setC(int C) {
        this.C = C;
  }
  
  public int getC() {
        return this.C;
  }
  
  public void setN(int N) {
        this.N = N;
  }
  
  public int getN() {
        return this.N;
  }
  
  public void setWarningColor(color warningColor) {
        this.warningColor = warningColor;
  }
  
  public int getWarningColor() {
        return this.warningColor;
  }
  
  public void setPntFontN(int pntFontN) {
        this.pntFontN = pntFontN;
  }
  
  public int getPntFontN() {
        return this.pntFontN;
  }
  
  public void setColN(color ColN) {
        this.ColN = ColN;
  }
  
  public color getColN() {
        return this.ColN;
  }
  public color getColCella() {
        return this.ColCella;
  }
  
  public void setFissa(boolean fissa) {
        this.fissa = fissa;
  }
  
  public boolean isFissa() {
        return this.fissa;
  }
}