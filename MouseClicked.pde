void mouseClicked() {
    //println(mouseX);
    //println(mouseY);
    //println(mouseButton);
    //deseleziono ogni cella, trovo se ce n era una ancora cliccata e in tal caso salvo num riga e col
    if(mouseButton == 37){ //clic sinistro{
      for(int r = 0; r < cellePerLato; r++){
        for(int c = 0; c < cellePerLato; c++){
          if(celle[r][c].isSelezionata()){
            UCS.setR(r);
            UCS.setC(c);
            UCS.setSelezionata(true);
          }
          celle[r][c].setSelezionata(false);
        }
      }
      
      //inverto lo stato di selezionata della cella cliccata, se era già selezionata la deseleziono
      int rigaSel = floor(mouseX / (lato/cellePerLato));
      int colonnaSel = floor(mouseY / (lato/cellePerLato));
      
      if(rigaSel == UCS.getR() && colonnaSel == UCS.getC() && !UCS.isSelezionata()){
        celle[rigaSel][colonnaSel].setSelezionata(true);
         UCS.setSelezionata(true);
        //println("riseleziono");
      }else if(rigaSel == UCS.getR() && colonnaSel == UCS.getC()){
        celle[rigaSel][colonnaSel].setSelezionata(false);
        UCS.setSelezionata(false);
        //println("deseleziono");
      }else{
        celle[rigaSel][colonnaSel].setSelezionata(true);
        UCS.setSelezionata(true);
        //println("seleziono");
      }
      
      UCS.setR(rigaSel);
      UCS.setC(colonnaSel);
    }
    else if (mouseButton == 39){
      //trovo la cella selezionata
      int rigaSel = floor(mouseX / (lato/cellePerLato));
      int colonnaSel = floor(mouseY / (lato/cellePerLato));
      
      celle[rigaSel][colonnaSel].toggleFissa();
    }
  
}