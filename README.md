# README #

#Requirement:

[Processing 3.1+](https://processing.org/download/)

#Version 1.0

* Select cells with left clict, move with keyboard arrows 

* pres Del to delete cells 

* press R to resolve

* you cant write number in not allowed positions

#Installation and run

Download processing 3

run it

This is a Processing 3 Project. You need P3 libraries to compile and run the code.


#Description

Start with an empty map.

Write some number into the map.

![Scheme](images/main.png)

Press R

![Scheme](images/resolv.png)

Good work!


#License 

All my work is released under [DBAD](https://www.dbad-license.org/) license.