public class thRisolutore extends Thread{
  private Cella[][] celleSoluzione;
  private boolean esecuzione; 
  private boolean arretra;
  
  public thRisolutore(Cella[][] celleInput) {
    celleSoluzione = new Cella[9][9];
    
    //creao un secondo array bidim celle dove andrò a salvare i numeri della soluzione
    //ora riempo l array soluzioni con i numeri gia inseriti e li metto fissi
    for(int r = 0; r < cellePerLato; r++){
      for(int c = 0; c < cellePerLato; c++){
        celleSoluzione[c][r] = new Cella(c, r, lato/cellePerLato);
        
        celleSoluzione[c][r].setN(celleInput[c][r].getN()); 
        
        if(celleSoluzione[c][r].getN() > 0){
          celleSoluzione[c][r].setFissa(true);
          //println("Riga" + r + "Colonna" + c + "è fissata: " + celleSoluzione[c][r].getN());
        }
      }
    }
    
    //faccio subito partire il thread
    this.inizia();
  }
  
  public void inizia(){
    this.esecuzione = true;
    this.start();
  }
  
  boolean isEsecuzione() {
  return this.esecuzione;
  }
  
  private boolean inserisciVal(int r, int c, int n){
    //println("provo con " + n);
    //Controllo in tutta la COLONNA se è già presente il numero che si vuole inserire
    for(int iC = 0; iC < 9; iC++){
      //println("c: " + iC);
      if(n != 0 && n == celleSoluzione[iC][r].getN()){
        //println(n + " gia presente in c: " + iC);
        
        return true;
      }//else println(n + " non presente in c: " + iC);
    }
    
    //Controllo in tutta la COLONNA se è già presente il numero che si vuole inserire
    for(int iR = 0; iR < 9; iR++){
      //println("r: " + iR);
      if(n != 0 && n == celleSoluzione[c][iR].getN()){
        //println(n + " gia presente in c: " + iR);
        
        return true;
      }//else println(n + " non presente in c: " + iR);
    }
    
    //Controllo che nella regione non è già presente lo stesso numero
    //trovo la regione della cella
    int cR = floor(c/3);
    int rR = floor(r/3);
    
    for(int iR = 0; iR <3; iR++){
      for(int iC = 0; iC <3; iC++){
        if(n != 0 && n == celleSoluzione[cR*3 + iC][rR*3 + iR].getN()){
          //controllo che cella modificata e controllata siano diverse
            if(c != (cR*3 + iC) && r != (rR*3 + iR)){
              return true;
            }
        }  
      }
    }
    
    //try { 
    //  Thread.sleep(500);
    //}catch(InterruptedException e) {
    //  esecuzione = false;
    //}
    
    celleSoluzione[c][r].setN(n);
    return false;
  }
  
  public void run()                       
  {     
    while(esecuzione){
      celle = celleSoluzione;
      boolean errore; //numero inserito non valido
      int prove;
      errore = true;
      try {
        celle = celleSoluzione;
        for(int r = 0; r < 9; r++){
          for(int c = 0; c < 9; c++){
            
            //println(r + " " + c);
            //parto dalla prima cella in alto a sx, controlo se è non è fissa altrimenti la salto
            if(!celleSoluzione[c][r].isFissa()){
              arretra = false;
              //println("cella non fissa: " + c);
              if(celleSoluzione[c][r].getN() < 9){
                  //println("n < 9: ");
                  errore = true;
                  prove = 1;
                  while(errore){
                    //quando arrivo a dover inserire 10 nella esco dal while e torno indietro
                    if(celleSoluzione[c][r].getN() + prove < 10){
                      //println("controllo fattibilità");
                      errore = inserisciVal(r, c, celleSoluzione[c][r].getN() + prove);
                      prove ++; 
                    }else{
                      //se arrivo a provare il 9 e non va allora arretro di una cella
                      arretra = true;
                      errore = false;
                      celleSoluzione[c][r].setN(0);
                    if(c!=0){
                      c -= 2;
                    }else{
                      r --;
                      c = 7;
                    }
                  }
                }
              }else{
                //se la cella in cui ho indietreggiato contiene nove allora arretro ancroa
                errore = false;
                arretra = true;
                celleSoluzione[c][r].setN(0);
                if(c!=0){
                  c -= 2;
                }else{
                  r --;
                  c = 7;
                }
              }
            }else if (arretra){
                //se ho appena arretrato e la cella è ancora fissa aretro ancora
                if(c!=0){
                  c -= 2;
                }else{
                  r --;
                  c = 7;
                }
            }
            //println("nuova cella colonna");
            Thread.sleep(0);
          }
          
          //println("nuova cella riga");
        }
        println("è stato facile");
        esecuzione = false;        
        
      }catch(InterruptedException e) {
          esecuzione = false;
      }
    }      
  }
}