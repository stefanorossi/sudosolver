public class ThTiming extends Thread {
  private boolean esecuzione;
  int r, c;
  
  public ThTiming(int r, int c) {
    this.esecuzione = false;
    this.r = r;
    this.c = c;
  }
  
public void inizia(){
    this.esecuzione = true;
    this.start();
}

boolean isEsecuzione() {
  return this.esecuzione;
}
  
void run () {
  //salvo il colore iniziale
  try {  
    celle[r][c].setWarningColor(color(255, 0, 0, 255));
    Thread.sleep(800);
  }catch(InterruptedException e) {
      esecuzione = false;
  }
   
  while (esecuzione) {
    for(int i = 0; i<=255 && esecuzione; i += 5){
      try {
        Thread.sleep(20);
        celle[r][c].setWarningColor(color(255, 0, 0, 255 - i));
        //println(i);
      }
      catch(InterruptedException e) {
          esecuzione = false;
      }
    }
    esecuzione = false;
    celle[r][c].setWarningColor(color(255, 0, 0, 0));
    }
  }
}